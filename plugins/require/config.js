require.config({
    baseUrl: '/static/plugins',
    waitSeconds: 10,
    paths: {
        'bootstrap': 'bootstrap/js/bootstrap.min',
        'bootstrap-select': 'bootstrap-select/js/bootstrap-select.min',
        'bootstrap-select_zh': 'bootstrap-select/js/i18n/defaults-zh_CN.min',
        'bootbox': 'bootbox/bootbox.min',
        'define': '../js/define',
        'dataTable': 'DataTables/media/js/jquery.dataTables.min',
        'datepicker_zh': 'bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min',
        'datepicker': 'bootstrap-datepicker/js/bootstrap-datepicker.min',
        'editor': 'editor/js/dataTables.editor.min',
        'fileinput.main': 'bootstrap-fileupload/fileinput.min',
        'fileinput.theme': 'bootstrap-fileupload/themes/fa/theme',
        'fileinput': 'bootstrap-fileupload/locales/zh',
        'history': 'jquery.history/jquery.history.min',
        'icheck': 'iCheck/icheck.min',
        'jquery': 'jQuery/jquery-2.1.4.min',
        'jquery.confirm': 'jquery.confirm/jquery.confirm',
        'juicer': 'juicer/juicer',
        'jstree': 'jstree/src/jstree',
        'jstree.checkbox': 'jstree/src/jstree.checkbox',
        'jstree.contextmenu': 'jstree/src/jstree.contextmenu',
        'jstree.search': 'jstree/src/jstree.search',
        'main': '../js/main',
        'moment': 'moment/min/moment.min',
        'mixitup': 'mixitup/mixitup',
        'owlCarousel': 'owl-carousel/owl-carousel/owl.carousel.min',
        'perfectScrollbarJQuery': 'perfect-scrollbar/js/perfect-scrollbar.jquery.min',
        'perfectScrollbar': 'perfect-scrollbar/js/perfect-scrollbar.min',
        'pnotify': 'pnotify/pnotify.custom',
        'pnotify.desktop': 'pnotify/pnotify.desktop',
        'pnotify.nonblock': 'pnotify/pnotify.nonblock',
        'pnotify.buttons': 'pnotify/pnotify.buttons',
        'scrollTo': 'jquery.scrollTo/jquery.scrollTo.min',
        'server': '../js/server',
        'serverGroup': '../js/server-group',
        'summernote': 'summernote/summernote-zh-CN',
        'summernote.main': 'summernote/summernote.min',
        'subview': '../js/subview',
        'selector': '../js/selector',
        'smartwizard': 'smartwizard/js/jquery.smartWizard',
        'tabs': 'multi-tabs/js/tabs',
        'tree': '../js/tree',
        'util': 'util',
        'util-datatable': 'util-datatable',
        'validate': 'jquery-validation/jquery.validate',
        'validate.cn': 'jquery-validation/localization/messages_zh.min',
        'validate.method': 'jquery-validation/additional-methods',
        'velocity': 'velocity/jquery.velocity.min',
        'vis': 'vis/vis-network.min',
        'smartview': '../js/smartview/smartview',
        'text': 'require/text',
        'ZeroClipboard': 'zeroclipboard/ZeroClipboard.min'
    },
    shim: {
        'jquery': {
            exports: "$"
        },
        'jquery.confirm': {
            deps: ['jquery','bootstrap'],
            exports: "$"
        },
        'juicer': {
            deps: ['jquery'],
            exports: "$"
        },
        'bootbox': {
            deps: ['jquery']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap-select': {
            deps: ['jquery', 'bootstrap'],
            exports: 'bootstrap-select'
        },
        'bootstrap-select_zh': {
            deps: ['jquery', 'bootstrap', 'bootstrap-select'],
            exports: 'bootstrap-select'
        },
        'dataTable': {
            exports: "$",
            deps: ['jquery', 'icheck']
        },
        'datepicker': {
            deps: ['jquery'],
            exports: 'datepicker'
        },
        'datepicker_zh': {
            deps: ['jquery', 'datepicker'],
            exports: "datepicker_zh"
        },
        'editor': {
            deps: ['jquery'],
            exports: "$"
        },
        'fileinput': {
            deps: ['jquery', 'fileinput.main', 'fileinput.theme', 'fileinput.theme']
        },
        'fileinput.theme': {
            deps: ['jquery', 'fileinput.main']
        },
        'history': {
            deps: ['jquery']
        },
        'icheck': {
            deps: ['jquery']
        },
        'uitl': {
            deps: ['jquery', 'jquery.confirm', 'main', 'define', 'pnotify', 'summernote', 'bootstrap-select_zh'],
            exports: "Util"
        },
        'util-datatable': {
            deps: ['jquery', 'dataTable']
        },
        'main': {
            deps: ['jquery', 'bootbox', 'define', 'history'],
            exports: "Main"
        },
        'moment': {
            deps: ['jquery'],
            exports: "moment"
        },
        'mixitup': {
            deps: ['jquery'],
            exports: "mixitup"
        },
        'perfectScrollbar': {
            deps: ['jquery', 'perfectScrollbarJQuery']
        },
        'pnotify': {
            deps: ['jquery'],
            exports: "PNotify"
        },
        'owlCarousel': {
            deps: ['jquery'],
            exports: "owlCarousel"
        },
        'subview': {
            deps: ['jquery'],
            exports: "subview"
        },
        'server': {
            deps: ['jquery', 'jquery.confirm', 'juicer', 'util', 'dataTable', 'main', 'validate', 'summernote', 'subview'],
            exports: "server"
        },
        'serverGroup': {
            deps: ['jquery', 'server']
        },
        'scrollTo': {
            deps: ['jquery']
        },
        'selector': {
            deps: ['jquery']
        },
        'summernote': {
            deps: ['jquery', 'bootstrap', 'summernote.main']
        },
        'smartwizard': {
            deps: ['jquery'],
            export: "SmartWizard"
        },
        'smartview': {
            deps: ['jquery','vis'],
            export: "smartview"
        },
        'tabs': {
            deps: ['jquery']
        },
        'tree': {
            deps: ['jquery', 'jquery.confirm', 'juicer', 'util', 'dataTable', 'main', 'jstree', 'jstree.contextmenu', 'jstree.search', 'jstree.checkbox'],
            exports: "tree"
        },
        'validate': {
            deps: ['jquery']
        },
        'velocity': {
            deps: ['jquery']
        },
        'vis': {
            export: "vis"
        },
        'ZeroClipboard': {
            export: "ZeroClipboard"
        }
    }
});
