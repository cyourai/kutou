/*!
 * jquery.confirm
 *
 * @version 2.3.1
 *
 * @author My C-Labs
 * @author Matthieu Napoli <matthieu@mnapoli.fr>
 * @author Russel Vela
 * @author Marcus Schwarz <msspamfang@gmx.de>
 *
 * @license MIT
 * @url https://myclabs.github.io/jquery.confirm/
 */
(function ($) {

    /**
     * Confirm a link or a button
     * @param [options] {{title, text, confirm, cancel, confirmButton, cancelButton, post, submitForm, confirmButtonClass}}
     */
    $.fn.confirm = function (options) {
        if (typeof options === 'undefined') {
            options = {};
        }

        this.click(function (e) {
            e.preventDefault();

            var newOptions = $.extend({
                button: $(this)
            }, options);

            $.confirm(newOptions, e);
        });

        return this;
    };

    /**
     * Show a confirmation dialog
     * @param [options] {{title, text, confirm, cancel, confirmButton, cancelButton, post, submitForm, confirmButtonClass}}
     * @param [e] {Event}
     */
    $.confirm = function (options, e) {
        // Do nothing when active confirm modal.
        if ($('.confirmation-modal').length > 0)
            return;
        /**
         * Globally definable rules
         */
        $.confirm.options = {
            text: "是否确定?",
            title: "提示",
            url: "",
            param: {},
            post: false,
            submitForm: false,
            modalStyle: '',
            button: {
                confirm: {
                    class: 'btn-warning width_middle',
                    text: '确 定'
                },
                clean: {
                    class: 'btn-info',
                    text: "取 消"
                }
            },
            callback: function () {
            },
            cleanback: function () {
            }
        };

        // Default options
        var settings = $.extend({}, $.confirm.options, {
            confirm: function () {
                var url = e && (('string' === typeof e && e) || (e.currentTarget && e.currentTarget.attributes['href'].value));
                if (url) {
                    if (options.post) {
                        var form = $('<form method="post" class="hide" action="' + url + '"></form>');
                        $("body").append(form);
                        form.submit();
                    } else {
                        window.location = url;
                    }
                } else {
                    settings.callback(settings.param);
                }
            },
            cancel: function () {
                if (settings.cleanback) settings.cleanback(settings.param);
            }
        }, options);

        // Modal
        var modalHeader = '';
        if (settings.title !== '') {
            modalHeader =
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">' + settings.title + '</h4>' +
                '</div>';
        }
        var modalHTML =
            '<div class="confirmation-modal modal fade" tabindex="-1" role="dialog">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content ' + settings.modalStyle + '">' +
            modalHeader +
            '<div class="modal-body">' + settings.text + '</div>' +
            '<div class="modal-footer"></div>' +
            '</div>' +
            '</div>' +
            '</div>';


        var modal = $(modalHTML);
        $("body").append(modal);
        // 追加按钮
        var confirmBtn = settings.button.confirm;
        var cleanBtn = settings.button.clean;
        $('.modal-footer').html('');
        if (confirmBtn) {
            $('.modal-footer').append('<button class="confirm btn ' + confirmBtn.class + '" type="button" data-dismiss="modal">' +
                confirmBtn.text +
                '</button>');
        }
        if (cleanBtn) {
            $('.modal-footer').append('<button class="clean btn ' + cleanBtn.class + '" type="button" data-dismiss="modal">' +
                cleanBtn.text +
                '</button>');
        }
        if (settings.height) {
            $('.modal-body').height(settings.height);
        }
        modal.on('shown.bs.modal', function () {
            modal.find(".btn-warning").focus();
        });
        modal.on('hidden.bs.modal', function () {
            modal.remove();
        });
        modal.find(".confirm").click(function () {
            settings.confirm(settings.button);
        });
        modal.find(".cancel").click(function () {
            settings.cancel(settings.button);
        });

        if (settings.url != '') {
            $.get(settings.url, settings.param, function (data, status) {
                if (status == 'success')
                    $('.modal-body').html(data);
                if (settings.hide != true) modal.modal('show');
                else
                    $('.modal-body').html('页面加载失败:' + settings);

            });
        } else {
            if (settings.hide != true) modal.modal('show');
        }
    };
})(jQuery);
