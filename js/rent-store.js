$(function () {
    $(".mask").click(function () {
        $(".mask").hide();
        $(".all").hide();
    });
    $(".right input").blur(function () {
        if ($.trim($(this).val()) == '') {
            $(this).addClass("place").html();
        }
        else {
            $(this).removeClass("place");
        }
    });
});
var arrFile = [];
function setImagePreview(fileObj) {
    var num = parseInt(fileObj.files.length);
    for (var i=0;i<num;i++){
        var reader = new FileReader();
        reader.readAsDataURL(fileObj.files[i]);
        var j = 1;
        $(reader).load(function () {
            while (true) {
                if ($("#imgadd-" + j).attr("src") != "images/imgadd.png") {
                    j++;
                } else {
                    break;
                }
            }
//                arrFile[j] = file;
            $("#imgadd-" + j).attr("src", this.result);
            $("#rm-imgadd-" + j ).show();
        });
    }

}
function removeImg(id) {
    $('#' + id).attr('src', 'images/imgadd.png');
    $("#rm-" + id).hide();
}