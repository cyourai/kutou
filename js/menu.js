$(document).ready(function() {
	// 过滤项控制
	$('.deactive').click(function(){
		var target = "#"+ $(this).attr('data-target');
		if($(target).hasClass('none')) {
			// 点其他
			$('.deactives').removeClass('deactives');
			$('.menu-context ul').addClass('none');
			$(this).toggleClass('deactives');
			$(target).toggleClass('none');
            $('.mask').show();
		} else {
			// 点自身
            closeMenu();
		}
	});
    $(".mask").click(function () {
        closeMenu();
    });
    $('.menu-context a').click(function(){
        closeMenu();
	});
});
function closeMenu() {
    $('.mask').hide();
    $('.deactives').removeClass('deactives');
    $('.menu-context ul').addClass('none');
};